﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace UAF_Service
{
    public class AutomatedTests
    {
        /// <summary>
        /// field of the path of the test framework for the product
        /// </summary>
        string dllPath;
        
        /// <summary>
        /// field of the path of the environments of the test framework
        /// </summary>
        string environmentsPath;
       
        /// <summary>
        /// Constructor of class
        /// </summary>
        public AutomatedTests()
        {
            string filePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\DllPath.txt";
            if (!File.Exists(filePath)) throw new FileNotFoundException($"DllPath.txt doesn't exist in {filePath}");

            using StreamReader streamReader = new(filePath);

            dllPath = streamReader.ReadLine();

            if (!File.Exists(dllPath)) throw new FileNotFoundException($"The test dll doesn't exist in folder {dllPath}");

            environmentsPath = streamReader.ReadLine();

            if (!File.Exists(environmentsPath)) throw new FileNotFoundException($"The environments file doesn't exist in folder {environmentsPath}");

        }

        /// <summary>
        /// Task that executes the defined commands to run the corresponding test framework
        /// </summary>
        /// <param name="product">Product to be tested</param>
        /// <param name="buildId">build version of the product</param>
        /// <returns></returns>
        internal Task Execute(string product, string buildId)
        {
            //TODO: Change this string
            string cmdText = $"/C dotnet test \"{dllPath}\" --filter TestCategory={product}";
            Process process = new();
            switch (product)
            {
                case "VQ":
                    ModifyEnvironmetFile(buildId);
                    EventLog.WriteEntry("UAF Service", $"Starting automation on {product}, build {buildId}", EventLogEntryType.Information);
                    process = Process.Start("CMD.exe", cmdText);
                    break;
                default:
                    break;
            }
            return process.WaitForExitAsync();
        }

        /// <summary>
        /// Method that changes the environmente on the test framework to the one to be tested
        /// </summary>
        /// <param name="buildId">build version</param>
        private void ModifyEnvironmetFile(string buildId)
        {
            try
            {

                var json = File.ReadAllText(environmentsPath);
                var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                dictionary["FWQA"] = buildId;
                json = JsonConvert.SerializeObject(dictionary, Formatting.Indented);

                File.WriteAllText(environmentsPath, json);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Application", ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}