﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UAF_Service
{
    public static class Data
    {
        private const string username = "SQL_UAF_Azure";
        private const string password = "5nKImPT3lMcyDDGlqLEhUMLm5rQVWcglXRE3NwlF5kfTRgXMPxxD623DDnX8XynG";
        private const string database = "UAF";
        private const string serverName = "qasql-derive.database.windows.net";

        private static readonly string runsFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\AutomationRuns.json";
        private static JObject ProductInfo = new();

        internal static bool BuildExists(string product, string buildId)
        {
            //TODO check that the build exists in DB
            ProductInfo = LoadJson();
            var lastBuild = ProductInfo.SelectToken("products").First(x => x.SelectToken("name").Value<string>() == product).SelectToken("lastBuild").Value<string>();
            return lastBuild == buildId;
        }

        internal static void CreateBuild(string product, string buildId)
        {
            //TODO move this to DB
            var path = ProductInfo.SelectToken("products").First(x => x.SelectToken("name").Value<string>() == product).SelectToken("lastBuild");
            var date = ProductInfo.SelectToken("products").First(x => x.SelectToken("name").Value<string>() == product).SelectToken("date");
            path.Replace(buildId);
            date.Replace(DateTime.Now.ToString());
            string output = JsonConvert.SerializeObject(ProductInfo, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(runsFilePath, output);

        }

        internal static JObject LoadJson()
        {
            using StreamReader reader = new(runsFilePath);
            string json = reader.ReadToEnd();
            var item = JsonConvert.DeserializeObject<JObject>(json);
            return item;
        }

        
        //Leave this here for future use
        private static string ConnectionString()
        {
            return $"Data Source={serverName}; Initial Catalog={database}; User ID={username}; Password={password}; Trusted_Connection=false; MultipleActiveResultSets=true; Connection Timeout=600; Max Pool Size=777;";
        }

        private static SqlDataReader ExecuteQuery(string query)
        {
            var connectionString = ConnectionString();

            SqlConnection connection = new(connectionString);
            connection.Open(); // Opening SQL connection
            SqlCommand mycommand = new(query, connection);
            return mycommand.ExecuteReader();

            //TODO close the connection
        }
    }
}
