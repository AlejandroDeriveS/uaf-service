﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UAF_Service
{
    public class QueueService
    {
        /// <summary>
        /// Dictionary that store a list of task for each product
        /// </summary>
        readonly Dictionary<string, Queue<string>> tasks = new();

        /// <summary>
        /// Property of the AutomatedTest class
        /// </summary>
        readonly AutomatedTests automatedTests = new();

        /// <summary>
        /// Construcotr of the class
        /// </summary>
        public QueueService()
        {
            //Load products
            tasks.Add("VQ", new Queue<string>());
        }

        /// <summary>
        /// Method that verifies if the build existe or if it should added it to the queue
        /// </summary>
        /// <param name="product">Product to add to the queue</param>
        /// <param name="buildId">build version of the product</param>
        public void QueueBuild(string product, string buildId)
        {
            if (!Data.BuildExists(product, buildId))
            {
                Data.CreateBuild(product, buildId);

                tasks[product].Enqueue(buildId);

            }
        }

        /// <summary>
        /// Tast that monitors and runs the projects builds stored in the queue
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task ExecuteQueue(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                foreach (var product in tasks.Keys)
                {
                    if (tasks[product] != null && tasks[product]?.Any() == true)
                    {
                        await automatedTests.Execute(product, tasks[product].Dequeue());
                    }
                }

                await Task.Delay(TimeSpan.FromSeconds(15));
            }
        }
    }
}
