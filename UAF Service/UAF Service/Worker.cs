using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UAF_Service
{
    public class Worker : BackgroundService
    {
        /// <summary>
        /// Field of logger instance
        /// </summary>
        private readonly ILogger<Worker> _logger;

        /// <summary>
        /// Property of QueueService class
        /// </summary>
        private readonly QueueService QueueService = new();

        /// <summary>
        /// Constructor of class with logger instance
        /// </summary>
        /// <param name="logger">Logger instance</param>
        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Main entry point for the working task that executes the serivce
        /// </summary>
        /// <param name="stoppingToken">Token to control the service</param>
        /// <returns></returns>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Task task = new(delegate { QueueService.ExecuteQueue(stoppingToken); });
            task.Start();

            WorkerCycle(stoppingToken);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        /// <summary>
        /// Task that retrieves the last succesful builds from the different projects
        /// </summary>
        /// <param name="stoppingToken">Token to control the service</param>
        /// <returns></returns>
        private async Task WorkerCycle(CancellationToken stoppingToken)
        {
            //Worker cycle
            while (!stoppingToken.IsCancellationRequested)
            {
                GetVQLatestBuild(stoppingToken);
                TimeSpan timeSpan = TimeSpan.FromMinutes(15);
#if DEBUG
                timeSpan = TimeSpan.FromSeconds(15);
#endif
                await Task.Delay(timeSpan, stoppingToken);
            }
        }

        /// <summary>
        /// Method that gets the last sucessful build from Jenkins for VQ
        /// </summary>
        /// <param name="stoppingToken">Token to control the service</param>
        private void GetVQLatestBuild(CancellationToken stoppingToken)
        {
            try
            {

                var baseAddress = new Uri("https://vq-ci.derivesystems.com/view/VQ/job/VQ_Firmware_Release/lastSuccessfulBuild/api/json");
                JToken jsonResponse;
                var userName = "jack.girem@derivesystems.com";
                var token = "11a4d5e168ce5bba1db0c0ff8625fd61e7";
                var authToken = Encoding.ASCII.GetBytes($"{userName}:{token}");

                using var client = new HttpClient() { BaseAddress = baseAddress };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authToken));
                var response = client.GetAsync(baseAddress, stoppingToken).Result;
                jsonResponse = JsonConvert.DeserializeObject<JToken>(response.Content.ReadAsStringAsync(stoppingToken).Result);

                var parameters = jsonResponse.SelectToken("actions").First(x => x.SelectToken("_class").Value<string>() == "hudson.model.ParametersAction").SelectToken("parameters");
                var majorVersion = parameters.First(x => x.SelectToken("name").Value<string>() == "FIRMWARE_VERSION_MAJOR").SelectToken("value");
                var minorVersion = parameters.First(x => x.SelectToken("name").Value<string>() == "FIRMWARE_VERSION_MINOR").SelectToken("value");
                var buildVersion = parameters.First(x => x.SelectToken("name").Value<string>() == "FIRMWARE_VERSION_BUILD").SelectToken("value");
                var fullVersion = $"1.1.{majorVersion}.{minorVersion} build {buildVersion}";

                EventLog.WriteEntry("UAF Service", $"Latest version {fullVersion}, of VQ", EventLogEntryType.Information);

                QueueService.QueueBuild("VQ", fullVersion);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("UAF Service", ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
